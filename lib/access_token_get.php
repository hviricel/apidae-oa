<?php

if ( !isset( $GLOBALS[ 'OA_API_ENV' ] ) )
{
  $GLOBALS[ 'OA_API_ENV' ] = 'production';
}

function access_token_get( $secret )
{
  $route = $GLOBALS[ 'OA_API_ENV' ] !== 'development' ? 'https://api.openagenda.com/v1/requestAccessToken' : 'https://dapi.openagenda.com/frontend_dev.php/v1/requestAccessToken';

  $ch = curl_init();

  if ( $GLOBALS[ 'OA_API_ENV' ] === 'development' )
  {
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  }

  curl_setopt( $ch, CURLOPT_URL, $route );
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_POST, true);

  curl_setopt($ch, CURLOPT_POSTFIELDS, array(
	  'grant_type' => 'authorization_code',
	  'code' => $secret
  ) );

  $received_content = curl_exec($ch);

  $ret = json_decode( $received_content, true )["access_token"];

  return $ret;
}
