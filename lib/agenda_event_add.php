<?php

if ( !isset( $GLOBALS[ 'OA_API_ENV' ] ) )
{
  $GLOBALS[ 'OA_API_ENV' ] = 'production';
}

function agenda_event_add( $accessToken, $agendaUid, $eventUid, $options = array() )
{
  extract( array_merge( array(
    'state' => 'published',
    'category' => null
  ), $options ) );


  $route = $GLOBALS[ 'OA_API_ENV' ] !== 'development' ? 
    "https://api.openagenda.com/v1/agendas/$agendaUid/events" : 
    "https://dapi.openagenda.com/frontend_dev.php/v1/agendas/$agendaUid/events";

  $ch = curl_init();

  if ( $GLOBALS[ 'OA_API_ENV' ] === 'development' )
  {
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  }

  curl_setopt( $ch, CURLOPT_URL, $route );
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_POST, true);

  curl_setopt($ch, CURLOPT_POSTFIELDS, array(
   'access_token' => $accessToken,
   'nonce' => rand(),
   'data' => json_encode( array(
      'event_uid' => $eventUid,
      'state' => $state,
      'category' => $category
    ) )
  ) );
  
  $received_content = curl_exec($ch);

  return json_decode( $received_content, true );
}