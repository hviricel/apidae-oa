<?php

if ( !isset( $GLOBALS[ 'OA_API_ENV' ] ) )
{
  $GLOBALS[ 'OA_API_ENV' ] = 'production';
}

function event_get( $key, $eventUid, $options = array() )
{
  extract( array_merge( array(
    'format' => 'v2'
  ), $options ) );


  $route = $GLOBALS[ 'OA_API_ENV' ] !== 'development' ? 
    "https://api.openagenda.com/v1/events/$eventUid?key=$key&format=$format" : 
    "https://dapi.openagenda.com/frontend_dev.php/v1/events/$eventUid?key=$key&format=$format";

  $ch = curl_init();

  if ( $GLOBALS[ 'OA_API_ENV' ] === 'development' )
  {
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  }

  curl_setopt( $ch, CURLOPT_URL, $route );
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  
  $received_content = curl_exec( $ch );

  $result = json_decode( $received_content, true );

  return $result[ 'data' ];
}